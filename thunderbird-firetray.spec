#%global commit0 275a3c9f648787f80f16e8ba33fd99de59c9cd67
#%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global moz_extensions %{_datadir}/mozilla/extensions

%global firefox_app_id \{ec8030f7-c20a-464f-9b0e-13a3a9e97384\}
%global src_ext_id \{9533f794-00b4-4354-aa15-c2bbda6989f8}
%global inst_dir %{moz_extensions}/%{firefox_app_id}/%{src_ext_id}

%global thunderbird_app_id \{3550f703-e582-4d05-9a08-453d09bdfdc6\}
%global thun_link_dir %{moz_extensions}/%{thunderbird_app_id}

%global seamonkey_app_id \{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}
%global sym_link_dir %{moz_extensions}/%{seamonkey_app_id}

Name:           thunderbird-firetray
Version:        0.6.1
Release:        0.1%{dist}
Summary:        A system tray addon for thunderbird

Group:          Applications/Internet
License:        GPLv2 and CC-BY-SA
URL:            https://addons.mozilla.org/en-US/firefox/addon/4868
Source0:	https://github.com/foudfou/FireTray/archive/v%{version}.tar.gz

# Patch1 : Add Custom Prefs
Patch1:		firetray-custom-prefs.patch
Patch2:		firetray-version-60.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  dos2unix unzip perl
Requires:       mozilla-filesystem
AutoReqProv:    no
BuildArch:   noarch

%description
Firetray is a system tray addon for thunderbird, current features are :

- Show number of unread mails in the tray icon (Thunderbird, Seamonkey)
- close button hides to tray
- ability to start minimized to tray
- when restoring if the window is iconified, deiconify it
- Custom tray icon
- Hotkey for Hide/Unhide
- Handle mouse scrolls on tray icon

%prep
%setup -n FireTray-%{version}
%patch1 -p1 -b .custom-prefs
%patch2 -p1 -b .updateto-60

%build
cd src
make build

%install
rm -rf %{buildroot}
perl -pi -e 's/>38.0/>60.0/g' install.rdf
mkdir -p %{buildroot}%{inst_dir}
cp -rf ./build-/* %{buildroot}%{inst_dir}
chmod -R 755 %{buildroot}%{inst_dir}

# symlink from thunderbird extension to firefox extension
mkdir -p %{buildroot}%{thun_link_dir}
ln -s %{inst_dir} %{buildroot}%{thun_link_dir}

# symlink from seamonkey extension to firefox extension
mkdir -p %{buildroot}%{sym_link_dir}
ln -s %{inst_dir} %{buildroot}%{sym_link_dir}

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{inst_dir}
%{thun_link_dir}
%{sym_link_dir}


%changelog
* Mon Sep 10 2018 JMiahMan <JMiahMan@unity-linux.org> - 0.6.1-0.1
- Initial build
